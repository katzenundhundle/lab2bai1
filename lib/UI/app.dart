import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../Validator/mixin_login_validator.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(
              margin: EdgeInsets.only(top: 20.0),
            ),
            SurnameField(),
            Container(
              margin: EdgeInsets.only(top: 40.0),
            ),
            NameField(),
            Container(
              margin: EdgeInsets.only(top: 40.0),
            ),
            DateField(),
            Container(
              margin: EdgeInsets.only(top: 40.0),
            ),
            AddressField(),
            Container(
              margin: EdgeInsets.only(top: 40.0),
            ),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Email address'),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Pls input valid email.";
        }

        return null;
      },
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.password), labelText: 'Password'),
      validator: (value) {
        if (value!.length < 4) {
          return "Password has at least 4 characters.";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget DateField() {
    return TextFormField(
      obscureText: true,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly,
      ],
      decoration: InputDecoration(
          icon: Icon(Icons.calendar_today), labelText: 'Year of birth'),
      validator: (value) {
        if (value!.length < 4) {
          return "Years has at least 4 characters.";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget NameField() {
    return TextFormField(
      obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
      validator: (value) {
        if (value!.length < 1) {
          return "Name has at least 1 characters.";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget AddressField() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.location_city_sharp), labelText: "Address"),
      validator: validateAddress,
      onSaved: (value) {
        print('onSaved: value=$value');
      },
    );
  }

  Widget SurnameField() {
    return TextFormField(
      obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
      validator: (value) {
        if (value!.length < 1) {
          return "Name has at least 1 characters.";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('Demo only: password=$password');
          }
        },
        child: Text('Login'));
  }
}
